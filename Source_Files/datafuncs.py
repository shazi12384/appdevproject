# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=consider-using-enumerate
import pandas as pd
# import numpy as np


def dataread(file_path):
    df_load = pd.read_csv(file_path)
    return df_load


def row_drop(data_frame):
    data_frame2 = data_frame.drop([3378, 7512, 20650])
    return data_frame2


def making_years(split_a):
    split_a = int(split_a.split('/')[2])
    return split_a


def year_column(data_frame):
    data_frame = row_drop(data_frame)
    # df['Year']=[int(x.split('/')[2]) for x in df.iloc[:,0]]
    data_frame['Year'] = [making_years(x) for x in data_frame.iloc[:, 0]]
    return data_frame


def unique_years(data_frame):
    lst_years = list(data_frame['Year'].unique())
    return lst_years


def count_quakes(data_frame, lst_years):
    count_years = []
    split_a = 0
    for year in lst_years:
        y_r = year
        for i in range(len(data_frame)):
            if data_frame[i] == y_r:
                split_a = split_a+1
        count_years.append(split_a)
        split_a = 0
    # print(count_years)
    return count_years


def make_frame(lst1, lst2, col1, col2):
    data_frame = pd.DataFrame(list(zip(lst1, lst2)), columns=[col1, col2])
    return data_frame
