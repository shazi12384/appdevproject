# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=no-name-in-module
# pylint: disable=consider-using-enumerate
# pylint: disable=too-many-locals


def style(p_lot):
    # title
    p_lot.title.align = 'center'
    p_lot.title.text_font_size = '20pt'
    p_lot.title.text_font = 'serif'
    # Axis titles
    p_lot.xaxis.axis_label_text_font_size = '14pt'
    p_lot.xaxis.axis_label_text_font_style = 'bold'
    p_lot.yaxis.axis_label_text_font_size = '14pt'
    p_lot.yaxis.axis_label_text_font_style = 'bold'
    # Tick Labels
    p_lot.xaxis.major_label_text_font_size = '12pt'
    p_lot.yaxis.major_label_text_font_size = '12pt'
    # Plot the legend in the top left conren
    p_lot.legend.location = 'top_left'
    # print(p_lot)
    return p_lot
