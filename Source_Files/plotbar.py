# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=consider-using-enumerate
# import pandas as pd
# import numpy as np
# from bokeh.io import output_notebook, output_file, saving, save
from bokeh.plotting import figure, ColumnDataSource
import style as st
# from bokeh.models.tools import HoverTool
# import datafuncs as dfn


def plot_bar(years_list, counts_list, df_quake_freq):
    # Show the plot embedded in jupyter notebook
    # output_notebook()
    # Load the datasource
    cds = ColumnDataSource(data=dict(
        yrs=years_list,
        numQuakes=counts_list))
    # Tooltip
    tool_tips = [
        ("Years", "@yrs"),
        ("Number of earthquakes", "@numQuakes")]
    # Create a figure
    bar_chart = figure(title='Seismic Movement Frequencies',
                       plot_height=400,
                       plot_width=1000,
                       x_axis_label='Years',
                       y_axis_label='Occurances',
                       x_minor_ticks=2,
                       y_range=(0, df_quake_freq['Counts'].max() + 100),
                       toolbar_location=None,
                       tooltips=tool_tips)
    bar_chart.vbar(x='yrs', bottom=0, top='numQuakes',
                   color='#B6A905', width=0.75,
                   legend_label='Year', source=cds)
    # Style the bar chart
    bar_chart = st.style(bar_chart)
    # show(barChart)
    # save(barChart,filename="BarChart.html",title="Bar Chart")
    return bar_chart
