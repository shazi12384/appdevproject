# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# import pandas as pd
# import numpy as np
from bokeh.plotting import figure, ColumnDataSource
import style as st
# from bokeh.io import output_notebook, output_file
# from bokeh.models.tools import HoverTool
# import math
# from math import pi
# from bokeh.palettes import Category20c
# from bokeh.transform import cumsum


def plot_magnitude(df_quake_freq, df_load, years_list):
    # Show the plot embedded in jupyter notebook
    # output_notebook()
    magnitude = []
    # Get the average magnitude value for each year
    for i in df_quake_freq.Years:
        x_x = df_load[df_load['Year'] == i]
        data_magnitude = sum(x_x.Magnitude)/len(x_x.Magnitude)
        magnitude.append(data_magnitude)
    df_quake_freq['Magnitude'] = magnitude
    depth = []
    # get the average depth value for each year
    for i in df_quake_freq.Years:
        x_x = df_load[df_load['Year'] == i]
        data_depth = sum(x_x.Depth)/len(x_x.Depth)
        depth.append(data_depth)
    df_quake_freq['Depth'] = depth
    # Get the maximum earthquake magnitude for each year
    max_magnitude = list(df_load.groupby('Year').Magnitude.max())
    df_quake_freq['Max_Magnitude'] = max_magnitude
    # Load the datasource
    cds = ColumnDataSource(data=dict(
        yrs=years_list,
        avg_mag=df_quake_freq['Magnitude'].values.tolist(),
        max_mag=df_quake_freq['Max_Magnitude'].values.tolist()
    ))
    # Tooltips
    tool_tips = [
        ("Year", " @yrs"),
        ("Average Magnitude", " @avg_mag"),
        ("Maximum Magnitude", " @max_mag")
    ]
    # Create the figure
    m_p = figure(title='Seismic Values by Year (Max And Average)',
                 plot_width=800,
                 plot_height=500,
                 x_axis_label='Years',
                 y_axis_label='Magnitude',
                 x_minor_ticks=2,
                 y_range=(5, df_quake_freq['Max_Magnitude'].max() + 1),
                 toolbar_location=None,
                 tooltips=tool_tips)
    # Max Magnitude
    m_p.line(x='yrs', y='max_mag', color='#CD025C',
             line_width=2, legend='Max Magnitude', source=cds)
    # Average Magnitude
    m_p.line(x='yrs', y='avg_mag', color='#A30F7F',
             line_width=2, legend='Avg Magnitude', source=cds)
    m_p = st.style(m_p)
    # show(m_p)
    return m_p
