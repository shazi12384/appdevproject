# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=consider-using-enumerate
from math import pi
import pandas as pd
# import numpy as np
# from bokeh.io import output_notebook, output_file
from bokeh.plotting import figure
# from bokeh.models.tools import HoverTool
# import math
from bokeh.palettes import PiYG
from bokeh.transform import cumsum
import style2 as st


def plot_dougnut(df_load):
    # Show the plot embedded in jupyter notebook
    # output_notebook()
    x_x = dict(df_load['Type'].value_counts())
    pie_data = pd.Series(x_x).reset_index(
        name='value').rename(
            columns={'index': 'type'})
    pie_data['angle'] = pie_data['value']/pie_data['value'].sum() * 2*pi
    pie_data['color'] = PiYG[len(x_x)]
    # print(pie_data)
    p_final = figure(title='Seismic Movement Types from 1965-2016',
                     plot_height=400,
                     toolbar_location=None,
                     tools='hover',
                     tooltips="@type: @value",
                     x_range=(-0.5, 1.0))
    p_final.annular_wedge(x=0, y=1, inner_radius=0.2, outer_radius=0.35,
                          start_angle=cumsum(
                              'angle', include_zero=True),
                          end_angle=cumsum('angle'),
                          line_color='white',
                          fill_color='color', legend='type', source=pie_data)
    p_final.axis.axis_label = None
    p_final.axis.visible = False
    p_final.grid.grid_line_color = None
    p_final = st.style2(p_final)
    # show(p)
    return p_final
