# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=consider-using-enumerate
# import pandas as pd
# import numpy as np
# import style as st
from bokeh.io import save
# from bokeh.plotting import figure, show, ColumnDataSource
# from bokeh.models.tools import HoverTool
# import math
# from math import pi
# from bokeh.palettes import Category20c
# from bokeh.transform import cumsum
from bokeh.layouts import gridplot


def grid_plot(pmap, pmag, pdon, pbar, pline):
    # output_file('dashboard.html')
    # Make a grid
    grid = gridplot([[pmap], [pmag], [pdon], [pbar], [pline]])
    # Show the plot
    save(grid, filename="index.html", title="Dashboard")
    return grid
