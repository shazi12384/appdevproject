# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=no-name-in-module
# pylint: disable=consider-using-enumerate
# pylint: disable=too-many-locals
import math
from bokeh.plotting import figure, ColumnDataSource
# from bokeh.tile_providers import CARTODBPOSITRON, STAMEN_TERRAIN
from bokeh.tile_providers import get_provider, Vendors
get_provider(Vendors.CARTODBPOSITRON)
get_provider(Vendors.STAMEN_TERRAIN)
# get_provider('CARTODBPOSITRON')


# Create Geo Map Plot
def plot_map(df_load):
    # Show the plot embedded in jupyter notebook
    # output_notebook()
    lat = df_load['Latitude'].values.tolist()
    lon = df_load['Longitude'].values.tolist()
    lst_lat = []
    lst_lon = []
    i = 0
    # Convert lat and long values inot merc_projection
    for i in range(len(lon)):
        r_major = 6378137.000
        x_x = r_major * math.radians(lon[i])
        scale = x_x/lon[i]
        y_y = 180.0/math.pi * math.log(math.tan(math.pi/4.0 +
                                                lat[i] *
                                                (math.pi/180.0)/2.0))*scale
        lst_lon.append(x_x)
        lst_lat.append(y_y)
        i += 1
    df_load['coords_x'] = lst_lat
    df_load['coords_y'] = lst_lon
    lats = df_load['coords_x'].tolist()
    longs = df_load['coords_y'].tolist()
    mags = df_load['Magnitude'].tolist()
    # Create datasource
    cds = ColumnDataSource(data=dict(
        lat=lats,
        lon=longs,
        mag=mags
    ))
    # Tooltip
    tool_tips = [
        ("Magnitude", " @mag")
    ]
    # Create figure
    p_lot = figure(title='Seismic Movements Mapping',
                   plot_width=1000,
                   plot_height=500,
                   x_range=(-2000000, 6000000),
                   y_range=(-1000000, 7000000),
                   tooltips=tool_tips)
    p_lot.add_tile(get_provider('STAMEN_TERRAIN'))
    p_lot.circle(x='lon', y='lat', fill_color='#000000',
                 fill_alpha=1, source=cds,
                 legend_label='Quakes 1965-2016', size=5)
    # p.add_tile(CARTODBPOSITRON)
    # Style the map plot
    # Title
    p_lot.title.align = 'center'
    p_lot.title.text_font_size = '20pt'
    p_lot.title.text_font = 'serif'
    # Legend
    p_lot.legend.location = 'bottom_right'
    p_lot.legend.background_fill_color = 'black'
    p_lot.legend.background_fill_alpha = 0.8
    p_lot.legend.click_policy = 'hide'
    p_lot.legend.label_text_color = 'white'
    p_lot.xaxis.visible = False
    p_lot.yaxis.visible = False
    p_lot.axis.axis_label = None
    p_lot.axis.visible = False
    p_lot.grid.grid_line_color = None
    # show(p_lot)
    return p_lot
