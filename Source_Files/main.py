# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# from bokeh.io import output_notebook, output_file
from bokeh.plotting import ColumnDataSource
# from bokeh.models.tools import HoverTool
import datafuncs as dfn
import plotbar as pb
import plotline as pl
import plotdougnut as don
import plotmagnitude as plm
import plotmap as plmp
import grid as gd
# import pandas as pd
# import numpy as np


def main():
    file_path = 'Dataset/database.csv'
    data_frame = dfn.dataread(file_path)
    # print(df.head(2))
    df2 = dfn.year_column(data_frame)
    # print(df2.head(3))
    lst1_years = dfn.unique_years(df2)
    # print(lst1)
    lst = list(df2["Year"])
    lst2_quakes = dfn.count_quakes(lst, lst1_years)
    # print(lst2_quakes)
    df_quake = dfn.make_frame(lst1_years, lst2_quakes, "Years", "Counts")
    # print(df_quake_freq.head(5))
    return df_quake


def year_df():
    file_path = 'Dataset/database.csv'
    data_frame = dfn.dataread(file_path)
    # print(df.head(2))
    df2 = dfn.year_column(data_frame)
    # print(df2.head(3))
    return df2


def year_list(df_freq):
    source_freq = ColumnDataSource(df_freq)
    # Create lists from source_freq ColumnDataSourcce
    years_list = source_freq.data['Years'].tolist()
    # counts_list = source_freq.data['Counts'].tolist()
    # print(source_freq)
    return years_list


def count_list(df_quake_freq2):
    source_freq = ColumnDataSource(df_quake_freq2)
    # Create lists from source_freq ColumnDataSourcce
    # years_list = source_freq.data['Years'].tolist()
    counts_list = source_freq.data['Counts'].tolist()
    # print(source_freq)
    return counts_list


if __name__ == "__main__":
    df_quake_freq = main()
    # print(df_quake_freq.head(4))
    df_year = year_df()
    # print(df_year.head(4))
    year_ls = year_list(df_quake_freq)
    # print(year_ls[0:10])
    count_ls = count_list(df_quake_freq)
    # print(count_ls[0:10])
    bar_plot = pb.plot_bar(year_ls, count_ls, df_quake_freq)
    line_plot = pl.plot_line(year_ls, count_ls, df_quake_freq)
    donut_plot = don.plot_dougnut(df_year)
    mag_plot = plm.plot_magnitude(df_quake_freq, df_year, year_ls)
    map_plot = plmp.plot_map(df_year)
    grid = gd.grid_plot(map_plot, mag_plot, donut_plot, bar_plot, line_plot)
