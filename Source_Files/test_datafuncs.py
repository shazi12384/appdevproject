#Unit testing

import datafuncs as dfn

def test_dataframe():
    str='Dataset/database.csv'
    assert dfn.dataread(str).size==491652
    assert dfn.dataread(str).shape==(23412,21)
    assert dfn.dataread(str).ndim==2
    
def test_rowdrops():
    #df=dfn.dataread()
    #df2=dfn.row_drop(df)
    str='Dataset/database.csv'
    assert dfn.row_drop(dfn.dataread(str)).iloc[3378]["ID"]!=dfn.dataread(str).iloc[3378]["ID"]
    assert dfn.row_drop(dfn.dataread(str)).iloc[7512]["ID"]!=dfn.dataread(str).iloc[7512]["ID"]
    assert dfn.row_drop(dfn.dataread(str)).iloc[20650]["ID"]!=dfn.dataread(str).iloc[20650]["ID"]
    
def test_making_year():
    assert dfn.making_years("01/01/1965")==1965
    assert dfn.making_years("01/01/2001")==2001
    
def test_yearcolumn():
    str='Dataset/database.csv'
    assert dfn.year_column(dfn.dataread(str)).columns[21]=="Year"
    ##assert dfn.year_column(dfn.dataread(str)).columns[20]=="Status"
    
def test_uniq():
    str='Dataset/database.csv'
    assert len(dfn.year_column(dfn.dataread(str))['Year'])>len(dfn.unique_years(dfn.year_column(dfn.dataread(str))))
    
def test_quakes_count():
    #df=dfn.year_column(dfn.dataread(str))
    #lst_years=dfn.unique_years(df)
    lst_years1=[1,2,3]
    df1=[1,2,2,2,3,3,3]
    lst_years2=[1,5,6]
    df2=[1,1,1,1,6,6,6,6,5,5]
    assert dfn.count_quakes(df1,lst_years1)==[1,3,3]
    assert dfn.count_quakes(df2,lst_years2)==[4,2,4]
    
def test_make_frame():
    lst1=["a","b","c","d","e"]
    lst2=[1,2,2,4,4]
    col1="Alpha"
    col2="Num"
    df=dfn.make_frame(lst1,lst2,col1,col2)
    assert df.size==len(lst1)*2
    assert df.shape==(len(lst1),2)
    assert df.ndim==2
    assert df.columns[0]==col1
    assert df.columns[1]==col2
