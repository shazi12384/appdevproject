# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# import datafuncs as dfn
# import pandas as pd
# import numpy as np
# from bokeh.io import output_notebook, output_file
from bokeh.plotting import figure, ColumnDataSource
import style as st
# from bokeh.models.tools import HoverTool


def plot_line(years_list, counts_list, df_quake_freq):
    # show the plot embeded in jypter notebook
    # output_notebook()
    # load the data source
    cds = ColumnDataSource(data=dict(
        yrs=years_list,
        numQuakes=counts_list
    ))
    # tooltips
    tool_tips = [
        ("Years", "@yrs"),
        ("Number of earth quakes", "@numQuakes")
    ]
    p_final = figure(title="Trend of Seismic Movements",
                     plot_width=800,
                     plot_height=400,
                     x_axis_label='Years',
                     y_axis_label='Occurances',
                     x_minor_ticks=2,
                     y_range=(0, df_quake_freq['Counts'].max()+100),
                     toolbar_location=None,
                     tooltips=tool_tips)
    p_final.line(x='yrs', y='numQuakes', color="#080605",
                 line_width=2, legend_label="Yearly trend", source=cds)
    # p_final.circle(x='yrs', y='numQuakes',
    # color="#009999", size=8, fill_color="white", source=cds)
    p_final = st.style(p_final)
    # show(p_final)
    # print(p_final)
    return p_final
